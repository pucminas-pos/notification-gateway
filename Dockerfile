FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/notification-gateway/src
COPY pom.xml /home/notification-gateway
RUN mvn -f /home/notification-gateway/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/notification-gateway/target/notification-gateway.jar /usr/local/lib/notification-gateway.jar
EXPOSE 8088
ENTRYPOINT ["java","-jar","/usr/local/lib/notification-gateway.jar"]