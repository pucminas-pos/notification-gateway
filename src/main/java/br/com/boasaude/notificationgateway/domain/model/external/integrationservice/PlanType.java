package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

public enum PlanType {

    MEDICAL, MEDICAL_ODONTOLOGY
}
