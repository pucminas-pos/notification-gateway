package br.com.boasaude.notificationgateway.domain.processor;

import br.com.boasaude.notificationgateway.domain.model.external.integrationservice.Reply;
import br.com.boasaude.notificationgateway.domain.model.external.integrationservice.Request;
import br.com.boasaude.notificationgateway.domain.model.internal.Email;
import br.com.boasaude.notificationgateway.infrastructure.email.EmailSender;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.types.InitializationInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ProcessRecordsInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownInput;
import com.amazonaws.services.kinesis.model.Record;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class ReplyStreamProcessor implements IRecordProcessor {

    private final ObjectMapper objectMapper;
    private final EmailSender emailSender;


    @Override
    public void initialize(InitializationInput initializationInput) {
        final String shard = initializationInput.getShardId();
        log.info("starting reply processor, shard: {}", shard);
    }

    @SneakyThrows
    @Override
    public void processRecords(ProcessRecordsInput processRecordsInput) {
        try {
            List<Record> records = processRecordsInput.getRecords();
            IRecordProcessorCheckpointer checkpointer = processRecordsInput.getCheckpointer();

            records.stream().forEach(record -> {
                try {
                    Reply reply = objectMapper.readValue(record.getData().array(), Reply.class);
                    Email email = buildReplyBody(reply);
                    //emailSender.send(email);
                    log.info("reply: {}", email);

                } catch (IOException e) {
                    log.error("error to parse", e);
                }
            });

            checkpointer.checkpoint();

        } catch (Exception ex) {
            log.error("error", ex);
        }
    }

    @Override
    public void shutdown(ShutdownInput shutdownInput) {
        try {
            IRecordProcessorCheckpointer checkpointer = shutdownInput.getCheckpointer();
            checkpointer.checkpoint();

        } catch (Exception ex) {
            log.error("error shutdown consumer kinesis");
        }
    }

    private Email buildReplyBody(Reply reply) {
        return Email.builder().to(reply.getEmail())
                .subject("Resposta recebida com sucesso")
                .text("Resposta recebida com sucesso. " +
                        "\nResposta: " + reply.getMessage() +
                        "\nProtocolo: " + reply.getProtocol()
                ).build();
    }
}
