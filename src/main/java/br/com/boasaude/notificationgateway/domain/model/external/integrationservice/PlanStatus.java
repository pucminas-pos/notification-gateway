package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

public enum PlanStatus {

    ACTIVE, SUSPENDED, INACTIVE

}
