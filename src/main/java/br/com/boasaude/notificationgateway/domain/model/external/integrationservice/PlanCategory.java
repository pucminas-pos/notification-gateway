package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

public enum PlanCategory {

    BASIC, INTERMEDIATE, VIP

}
