package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanRequest {

    private String email;
    private Plan newPlan;
    private Plan currentPlan;
}
