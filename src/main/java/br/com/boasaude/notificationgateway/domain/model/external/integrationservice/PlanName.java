package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

public enum PlanName {

    INDIVIDUAL, CORPORATE;

}
