package br.com.boasaude.notificationgateway.domain.processor;

import br.com.boasaude.notificationgateway.domain.model.external.integrationservice.PlanRequest;
import br.com.boasaude.notificationgateway.domain.model.internal.Email;
import br.com.boasaude.notificationgateway.infrastructure.email.EmailSender;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.types.InitializationInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ProcessRecordsInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownInput;
import com.amazonaws.services.kinesis.model.Record;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class PlanStreamProcessor implements IRecordProcessor {

    private final ObjectMapper objectMapper;
    private final EmailSender emailSender;


    @Override
    public void initialize(InitializationInput initializationInput) {
        final String shard = initializationInput.getShardId();
        log.info("starting plan processor, shard: {}", shard);
    }

    @SneakyThrows
    @Override
    public void processRecords(ProcessRecordsInput processRecordsInput) {
        try {
            List<Record> records = processRecordsInput.getRecords();
            IRecordProcessorCheckpointer checkpointer = processRecordsInput.getCheckpointer();

            records.stream().forEach(record -> {
                try {
                    PlanRequest planRequest = objectMapper.readValue(record.getData().array(), PlanRequest.class);

                    if (StringUtils.equalsIgnoreCase(record.getPartitionKey(), "CHANGING_PLAN")) {
                        Email changingEmail = buildChangingEmailBody(planRequest);
                        emailSender.send(buildChangingEmailBody(planRequest));
                        log.info("email: {}", changingEmail);

                    } else if (StringUtils.equalsIgnoreCase(record.getPartitionKey(), "CANCELING_PLAN")) {
                        Email cancelingEmail = buildCancelingEmailBody(planRequest);
                        emailSender.send(buildCancelingEmailBody(planRequest));
                        log.info("changingPlanRequest: {}", cancelingEmail);

                    } else {
                        log.error("discarding event. unknown partitionkey");
                    }
                } catch (IOException e) {
                    log.error("error to parse", e);
                }
            });

            checkpointer.checkpoint();

        } catch (Exception ex) {
            log.error("error", ex);
        }
    }

    @Override
    public void shutdown(ShutdownInput shutdownInput) {
        try {
            IRecordProcessorCheckpointer checkpointer = shutdownInput.getCheckpointer();
            checkpointer.checkpoint();

        } catch (Exception ex) {
            log.error("error shutdown consumer kinesis");
        }
    }

    private Email buildChangingEmailBody(PlanRequest planRequest) {
        return Email.builder().to(planRequest.getEmail())
                .subject("Solicitação de mudança de plano")
                .text("Solicitação de mudança de plano recebida com sucesso. " +
                        "\nEstaremos processando sua requisição e entraremos em contato" +
                        "\nPlano Atual" +
                        "\nNome: " + planRequest.getCurrentPlan().getName() +
                        " Tipo: " + planRequest.getCurrentPlan().getType() +
                        " Categoria: " + planRequest.getCurrentPlan().getCategory() +
                        "\n\nPlano Novo: " +
                        "\nNome: " + planRequest.getNewPlan().getName() +
                        " Tipo: " + planRequest.getNewPlan().getType() +
                        " Categoria: " + planRequest.getNewPlan().getCategory()
                ).build();
    }

    private Email buildCancelingEmailBody(PlanRequest planRequest) {
        return Email.builder().to(planRequest.getEmail())
                .subject("Solicitação de cancelamento de plano")
                .text("Solicitação de cancelamento de plano recebida com sucesso. " +
                        "\nEstaremos processando sua requisição e entraremos em contato" +
                        "\nPlano" +
                        "\nNome: " + planRequest.getCurrentPlan().getName() +
                        " Tipo: " + planRequest.getCurrentPlan().getType() +
                        " Categoria: " + planRequest.getCurrentPlan().getCategory()
                ).build();
    }
}
