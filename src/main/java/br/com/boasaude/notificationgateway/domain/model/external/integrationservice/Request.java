package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {

    private Long id;
    private String fullname;
    private String cpf;
    private String email;
    private String role;
    private String subject;
    private String message;
    private RequestType type;
    private String protocol;
    private LocalDateTime createdAt;
    private RequestStatus status;
    private List<Reply> replies;
}
