package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

public enum RequestStatus {

    OPENED, IN_PROGRESS, CLOSED;

}
