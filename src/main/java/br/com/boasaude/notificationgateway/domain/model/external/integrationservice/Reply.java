package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reply {

    private Long id;
    private String fullname;
    private String email;
    private String role;
    private String message;
    private String protocol;
    private LocalDateTime createdAt;
    private Request request;
}