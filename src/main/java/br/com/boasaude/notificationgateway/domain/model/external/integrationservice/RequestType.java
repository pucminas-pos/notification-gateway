package br.com.boasaude.notificationgateway.domain.model.external.integrationservice;

public enum RequestType {

    COMPLAINT, SUGGESTION, COMPLIMENT;


}
