package br.com.boasaude.notificationgateway.infrastructure.worker;

import br.com.boasaude.notificationgateway.infrastructure.factory.ReplyRecordProcessorFactory;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ReplyStreamWorkerBuilder {

    @Value("${application.name}")
    private String applicationName;

    @Value("${amazon.kinesis.stream.reply}")
    private String replyStreamName;

    private final ReplyRecordProcessorFactory replyRecordProcessorFactory;
    private final AmazonKinesis kinesisClient;
    private final AmazonDynamoDB amazonDynamoDB;
    private final AWSCredentialsProvider awsCredentialsProvider;
    private final AmazonCloudWatch amazonCloudWatch;

    @Async
    public void setup() {
        System.out.println("reply");
        KinesisClientLibConfiguration workerConfig = new KinesisClientLibConfiguration(
                applicationName + "-reply",
                replyStreamName,
                awsCredentialsProvider,
                applicationName + ":" + UUID.randomUUID()).withMaxRecords(1000).withIdleTimeBetweenReadsInMillis(500)
                .withInitialPositionInStream(InitialPositionInStream.TRIM_HORIZON);

        Worker worker = new Worker.Builder()
                .config(workerConfig)
                .recordProcessorFactory(replyRecordProcessorFactory)
                .kinesisClient(kinesisClient)
                .dynamoDBClient(amazonDynamoDB)
                .cloudWatchClient(amazonCloudWatch)
                .build();
        worker.run();
    }
}
