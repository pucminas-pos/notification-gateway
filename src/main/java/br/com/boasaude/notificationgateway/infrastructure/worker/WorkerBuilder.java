package br.com.boasaude.notificationgateway.infrastructure.worker;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class WorkerBuilder {

    private final PlanStreamWorkerBuilder planStreamWorkerBuilder;
    private final ReplyStreamWorkerBuilder replyStreamWorkerBuilder;
    private final RequestStreamWorkerBuilder requestStreamWorkerBuilder;

    @PostConstruct
    public void setup() {
        planStreamWorkerBuilder.setup();
        replyStreamWorkerBuilder.setup();
        requestStreamWorkerBuilder.setup();
    }

}
