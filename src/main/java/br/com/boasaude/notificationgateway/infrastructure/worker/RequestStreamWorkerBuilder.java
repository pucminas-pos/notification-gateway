package br.com.boasaude.notificationgateway.infrastructure.worker;

import br.com.boasaude.notificationgateway.infrastructure.factory.RequestRecordProcessorFactory;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class RequestStreamWorkerBuilder {

    @Value("${application.name}")
    private String applicationName;

    @Value("${amazon.kinesis.stream.request}")
    private String requestStreamName;

    private final RequestRecordProcessorFactory requestRecordProcessorFactory;
    private final AmazonKinesis kinesisClient;
    private final AmazonDynamoDB amazonDynamoDB;
    private final AWSCredentialsProvider awsCredentialsProvider;
    private final AmazonCloudWatch amazonCloudWatch;

    @Async
    public void setup() {
        System.out.println("request");
        KinesisClientLibConfiguration workerConfig = new KinesisClientLibConfiguration(
                applicationName+"-request",
                requestStreamName,
                awsCredentialsProvider,
                applicationName + ":" + UUID.randomUUID()).withMaxRecords(1000).withIdleTimeBetweenReadsInMillis(500)
                .withInitialPositionInStream(InitialPositionInStream.TRIM_HORIZON);

        Worker worker = new Worker.Builder()
                .config(workerConfig)
                .recordProcessorFactory(requestRecordProcessorFactory)
                .kinesisClient(kinesisClient)
                .dynamoDBClient(amazonDynamoDB)
                .cloudWatchClient(amazonCloudWatch)
                .build();
        worker.run();
    }
}
