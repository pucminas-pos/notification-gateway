package br.com.boasaude.notificationgateway.infrastructure.factory;

import br.com.boasaude.notificationgateway.domain.processor.ReplyStreamProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ReplyRecordProcessorFactory implements IRecordProcessorFactory {

    private final ReplyStreamProcessor replyStreamProcessor;

    @Override
    public IRecordProcessor createProcessor() {
        return replyStreamProcessor;
    }
}
